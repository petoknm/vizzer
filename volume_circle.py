from math import ceil, log10, sqrt, sin

class IIR2Filter(object):

    def __init__(self,order,cutoff,filterType):
        import numpy as np
        import scipy.signal as signal

        self.COEFFS = signal.butter(order,cutoff,filterType,output='sos')
        self.acc_input = np.zeros(len(self.COEFFS))
        self.acc_output = np.zeros(len(self.COEFFS))
        self.buffer1 = np.zeros(len(self.COEFFS))
        self.buffer2 = np.zeros(len(self.COEFFS))
        self.input = 0
        self.output = 0

    def filter(self,input):
        self.input = input
        self.output = 0

        #The for loop creates a chain of second order filters according to
        #the order desired. If a 10th order filter is to be created the
        #loop will iterate 5 times to create a chain of 5 second order
        #filters.
        for i in range(len(self.COEFFS)):
            self.FIRCOEFFS = self.COEFFS[i][0:3]
            self.IIRCOEFFS = self.COEFFS[i][3:6]

            #Calculating the accumulated input consisting of the input and
            #the values coming from the feedbaack loops (delay buffers
            #weighed by the IIR coefficients).
            self.acc_input[i] = (self.input + self.buffer1[i]
            * -self.IIRCOEFFS[1] + self.buffer2[i] * -self.IIRCOEFFS[2])

            #Calculating the accumulated output provided by the accumulated
            #input and the values from the delay bufferes weighed by the
            #FIR coefficients.
            self.acc_output[i] = (self.acc_input[i] * self.FIRCOEFFS[0]
            + self.buffer1[i] * self.FIRCOEFFS[1] + self.buffer2[i]
            * self.FIRCOEFFS[2])

            #Shifting the values on the delay line: acc_input->buffer1->
            #buffer2
            self.buffer2[i] = self.buffer1[i]
            self.buffer1[i] = self.acc_input[i]

            self.input = self.acc_output[i]

        self.output = self.acc_output[i]
        return self.output

lookupTable = [
    0x00,
    0x18,
    0x3c,
    0x7e,
    0xff
]

filter = IIR2Filter(2, [0.005, 0.3], 'bandpass')

def clip(x, xmin, xmax):
    return min(max(x, xmin), xmax)

def map(x, xmin, xmax, ymin, ymax):
    return ymin + (ymax-xmin)*(x-xmin)/(xmax-xmin)

def circle_byte(x, r):
    y_2 = r ** 2 - x ** 2
    y = sqrt(clip(y_2, 0, 100))
    y = clip(y, 0, 4)
    return lookupTable[int(y)]

frame = 0

def visualize(input):
    global frame
    frame = frame + 1
    x_center = 10 * sin(0.6 * frame / 43) + 12

    rms = input.rms
    rms = 10 * log10(rms + 1)
    rms = filter.filter(rms)
    # rms = clip(rms, 0, 100)
    # rms = abs(rms)
    return [circle_byte(x - x_center, rms) for x in range(0, 24)]
