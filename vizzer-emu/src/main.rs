extern crate byteorder;
extern crate cobs;
extern crate crc;
extern crate piston_window;
extern crate sdl2_window;

use std::io::{self, BufRead};
use std::sync::mpsc::channel;
use std::thread;

use byteorder::{BigEndian, ReadBytesExt};
use crc::crc16;
use piston_window::*;
use sdl2_window::*;

fn main() {
    let (tx, rx) = channel();

    thread::spawn(move || {
        let stdin = io::stdin();

        for frame in stdin.lock().split(0) {
            if let Ok(frame) = frame {
                let frame = decode(frame).unwrap();
                tx.send(frame).unwrap();
            }
        }
    });

    let mut window: PistonWindow<Sdl2Window> = WindowSettings::new("vizzer-emu", [240, 80])
        .exit_on_esc(true)
        .build()
        .unwrap();

    let mut state = [0x00u8; 24];
    let w = 10.0;

    while let Some(e) = window.next() {
        window.draw_2d(&e, |c, g| {
            clear([0.0, 0.0, 0.0, 1.0], g);

            for bit in 0..8 {
                for (col, val) in state.iter().enumerate() {
                    if val & (1 << bit) == 0 {
                        continue;
                    }
                    let x = col as f64 * w;
                    let y = bit as f64 * w;
                    rectangle([1.0; 4], [x, y, w, w], c.transform, g);
                }
            }
        });

        if e.update_args().is_some() {
            if let Ok(new_state) = rx.try_recv() {
                state = new_state;
            }
        }
    }
}

#[derive(Debug)]
enum Error {
    Io(io::Error),
    Cobs,
    CrcMismatch,
}

fn decode(mut frame: Vec<u8>) -> Result<[u8; 24], Error> {
    let len = cobs::decode_in_place(&mut frame).map_err(|_| Error::Cobs)?;
    let payload = &frame[..len - 2];
    let crc = (&frame[len - 2..len])
        .read_u16::<BigEndian>()
        .map_err(Error::Io)?;
    let crc_calculated = crc16::checksum_usb(payload);
    if crc != crc_calculated {
        Err(Error::CrcMismatch)
    } else {
        let mut state = [0; 24];
        state.copy_from_slice(payload);
        Ok(state)
    }
}
