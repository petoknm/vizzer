extern crate vizzer;
#[macro_use]
extern crate clap;
extern crate serial;
extern crate serde;
extern crate serde_json;
extern crate signal_hook;

use std::thread;
use std::fs;
use std::io::Write;
use std::sync::Arc;
use std::sync::atomic::{AtomicBool, Ordering};
use std::sync::mpsc::channel;
use std::time::{SystemTime, UNIX_EPOCH};

use serial::SerialPort;

use vizzer::input::PulseAudio;
use vizzer::output::{ClockV1, Encoder, CLOCKV1_SERIAL_SETTINGS};
use vizzer::{PreProcessor, ScriptingProcessor};

use clap::App;

use serde::Serialize;

const BUF_LEN: usize = 1024;

fn time_it<R, F: FnOnce() -> R>(name: &str, f: F) -> R {
    let start = SystemTime::now();
    let r = f();
    let end = SystemTime::now();

    #[derive(Serialize)]
    struct TimeInfo<'a> {
        name: &'a str,
        start: u128,
        end: u128,
    }

    let start = start.duration_since(UNIX_EPOCH).unwrap().as_millis();
    let end = end.duration_since(UNIX_EPOCH).unwrap().as_millis();

    println!("{}", serde_json::to_string(&TimeInfo { name, start, end }).unwrap());

    r
}

fn vec_to_array(v: Vec<u8>) -> Option<[u8; 24]> {
    if v.len() == 24 {
        let mut res = [0; 24];
        res.copy_from_slice(&v);
        Some(res)
    } else {
        None
    }
}

fn main() {
    let yaml = load_yaml!("cli.yml");
    let matches = App::from_yaml(yaml).get_matches();

    let output = matches.value_of("output").unwrap();
    let script = matches.value_of("script").unwrap().to_string();

    // Sampling stage
    let (tx_samples, rx_samples) = channel();
    thread::spawn(move || {
        let input = PulseAudio::new(BUF_LEN as u32).unwrap();

        loop {
            if let Ok(samples) = time_it("sampling", || input.read(BUF_LEN)) {
                tx_samples.send(samples).unwrap();
            }
        }
    });

    // Preprocessing stage
    let (tx_pp, rx_pp) = channel();
    thread::spawn(move || {
        let pp = PreProcessor::new(BUF_LEN);

        for samples in rx_samples {
            tx_pp.send(time_it("preprocessing", || pp.process(&samples))).unwrap();
        }
    });

    // Scripting stage
    let (tx_sp, rx_sp) = channel();
    thread::spawn(move || {
        let code = fs::read(script).unwrap();
        let code = String::from_utf8(code).unwrap();
        let sp = ScriptingProcessor::new(code).unwrap();

        for pp in rx_pp {
            if let Ok(sp) = time_it("scripting", || sp.process(pp)) {
                // let pp = &pp.fft[..24];
                // let sp = pp.iter().map(|&v| v as u8).collect();
                tx_sp.send(sp).unwrap();
            }
        }
    });

    // Signal handling
    let term = Arc::new(AtomicBool::new(false));
    signal_hook::flag::register(signal_hook::SIGTERM, Arc::clone(&term)).unwrap();
    signal_hook::flag::register(signal_hook::SIGINT,  Arc::clone(&term)).unwrap();

    // Output stage
    let mut output = serial::open(output).unwrap();
    output.configure(&CLOCKV1_SERIAL_SETTINGS).unwrap();

    for sp in rx_sp {
        if term.load(Ordering::Relaxed) {
            break;
        }
        // println!("{:?}", sp);
        time_it("output", || {
            let sp = vec_to_array(sp).unwrap();
            let frame = ClockV1::encode(&sp).unwrap();
            let _ = output.write(&frame);
            let _ = output.flush();
        })
    }
}
