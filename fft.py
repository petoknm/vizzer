from math import ceil, log10
from numpy import mean, clip

def toByte(n):
    res = 0
    for i in range(n):
        res >>= 1
        res |= 0x80
    return res

def map(x, xmin, xmax, ymin, ymax):
    return ymin + (ymax-xmin)*(x-xmin)/(xmax-xmin)

def downsample(arr, bin_size):
    bin_count = int(ceil(len(arr) / bin_size))
    pars = [(x*bin_size, (x+1)*bin_size) for x in range(bin_count)]
    return [mean(arr[start:end]) for (start, end) in pars]

def visualize(input):
    fft = downsample(input.fft, 8)
    fft = [10*log10(x) for x in fft[0:24]]
    fft = [map(x, -15, 60, 0, 8) for x in fft]
    fft = clip(fft, 0, 8)
    return [toByte(int(round(x))) for x in fft]
