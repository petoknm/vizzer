# vizzer

An interactive/scriptable audio visualization framework.

## Hardware

### ClockV1

A simple 24x8 LED matrix.

Communication protocol:
 - framing: COBS
 - frame: payload + crc16(usb)(x^16 + x^15 + x^2 + 1)
 - payload: 24bytes, each bytes is one column, going from left to right, LSB at the top, MSB as the bottom
