use super::Encoder;

use crc::crc16;
use serial::{BaudRate, CharSize, FlowControl, Parity, PortSettings, StopBits};

pub const SERIAL_SETTINGS: PortSettings = PortSettings {
    baud_rate: BaudRate::Baud115200,
    char_size: CharSize::Bits8,
    parity: Parity::ParityNone,
    stop_bits: StopBits::Stop1,
    flow_control: FlowControl::FlowNone,
};

pub struct Clock;

impl Encoder for Clock {
    type Payload = [u8; 24];
    type Error = ();

    fn encode(p: &[u8; 24]) -> Result<Vec<u8>, ()> {
        let crc = crc16::checksum_usb(p).to_be_bytes();

        let mut res = p.to_vec();
        res.extend_from_slice(&crc);

        let mut res = cobs::encode_vec(&res);
        res.push(0);

        Ok(res)
    }
}
