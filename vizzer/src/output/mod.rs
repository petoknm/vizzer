pub mod clock_v1;

pub use self::clock_v1::{Clock as ClockV1, SERIAL_SETTINGS as CLOCKV1_SERIAL_SETTINGS};

pub trait Encoder {
    type Payload;
    type Error;

    fn encode(p: &Self::Payload) -> Result<Vec<u8>, Self::Error>;
}
