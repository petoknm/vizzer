pub mod pulseaudio;

pub use self::pulseaudio::PulseAudio;

trait Input {
    type Error;

    fn read(&mut self, len: usize) -> Result<Vec<f32>, Self::Error>;
}
