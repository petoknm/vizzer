use psimple::Simple;
use pulse::def::BufferAttr;
use pulse::error::PAErr;
use pulse::sample::{Spec, SAMPLE_FLOAT32NE};
use pulse::stream::Direction;

pub struct PulseAudio {
    simple: Simple,
}

type PAResult<T> = Result<T, PAErr>;

impl PulseAudio {
    pub fn new(len: u32) -> PAResult<PulseAudio> {
        let spec = Spec {
            format: SAMPLE_FLOAT32NE,
            channels: 1,
            rate: 44100,
        };

        assert!(spec.is_valid());

        let buffer_attr = BufferAttr {
            maxlength: len,
            tlength: len,
            prebuf: len,
            minreq: len,
            fragsize: len,
        };

        let simple = Simple::new(
            None,               // Use the default server
            "led-visualizer",   // Our application’s name
            Direction::Record,  // We want a playback stream
            None,               // Use the default device
            "Music",            // Description of our stream
            &spec,              // Our sample format
            None,               // Use default channel map
            Some(&buffer_attr), // Use default buffering attributes
        )?;

        Ok(PulseAudio { simple })
    }

    pub fn read(&self, len: usize) -> PAResult<Vec<f32>> {
        let mut data = vec![0.0; len];
        unsafe {
            let data_ptr = data.as_mut_ptr() as *mut u8;
            let data = std::slice::from_raw_parts_mut(data_ptr, len * std::mem::size_of::<f32>());
            self.simple.read(data)
        }?;
        Ok(data)
    }
}
