extern crate cobs;
extern crate crc;
extern crate libpulse_binding as pulse;
extern crate libpulse_simple_binding as psimple;
extern crate pyo3;
extern crate rustfft;
extern crate serial;

pub mod input;
pub mod output;
pub mod preprocessing;
pub mod scripting;

pub use self::preprocessing::Processor as PreProcessor;
pub use self::scripting::Processor as ScriptingProcessor;

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
