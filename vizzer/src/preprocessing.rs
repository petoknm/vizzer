use super::scripting::Input as Output;
use rustfft::algorithm::Radix4;
use rustfft::num_complex::Complex;
use rustfft::FFT;
use std::f32::consts::PI;

pub struct Processor {
    fft: Radix4<f32>,
    window: Vec<f32>,
}

fn make_window(len: usize) -> Vec<f32> {
    (0..len)
        .map(|n| (PI * n as f32 / len as f32).cos())
        .collect()
}

impl Processor {
    pub fn new(len: usize) -> Processor {
        Processor {
            fft: Radix4::new(len, false),
            window: make_window(len),
        }
    }

    pub fn process(&self, samples: &[f32]) -> Output {
        let mut input: Vec<Complex<f32>> = samples.iter()
            .zip(&self.window)
            .map(|(s, w)| (s * w).into())
            .collect();
        let mut output = vec![Complex::<f32>::new(0.0, 0.0); input.len()];
        self.fft.process(&mut input, &mut output);
        let fft: Vec<f32> = output.iter().map(Complex::norm).collect();
        let rms: f32 = fft.iter().sum();
        Output { rms, fft }
    }
}
