use std::ffi::CString;

use pyo3::ffi;
use pyo3::prelude::*;
use pyo3::types::PyDict;
use pyo3::AsPyPointer;

#[pyclass]
pub struct Input {
    #[pyo3(get)]
    pub rms: f32,
    #[pyo3(get)]
    pub fft: Vec<f32>,
}

pub struct Processor {
    gil: GILGuard,
    python_globals: PyObject,
}

impl Processor {
    pub fn new(code: String) -> PyResult<Processor> {
        let gil = Python::acquire_gil();
        let py = gil.python();

        let python_globals = unsafe {
            let module_name = "script\0".as_ptr() as *const _;
            let module = ffi::PyModule_New(module_name);
            ffi::PyModule_AddStringConstant(
                module,
                "__file__\0".as_ptr() as *const _,
                "\0".as_ptr() as *const _,
            );

            let code = CString::new(code).unwrap();
            let globals = {
                let mptr = ffi::PyImport_AddModule("__main__\0".as_ptr() as *const _);
                ffi::PyModule_GetDict(mptr)
            };
            let locals = ffi::PyModule_GetDict(module);
            ffi::PyRun_StringFlags(
                code.as_ptr(),
                ffi::Py_file_input,
                globals,
                locals,
                ::std::ptr::null_mut(),
            );

            // copy all locals to the globals dictionary
            ffi::PyDict_Update(globals, locals);

            PyObject::from_owned_ptr_or_err(py, globals)?
        };

        Ok(Processor {
            gil,
            python_globals,
        })
    }

    pub fn process<T>(&self, input: Input) -> PyResult<T>
    where
        for<'obj> T: FromPyObject<'obj>,
    {
        let py = self.gil.python();
        let input = PyRef::new(py, input)?;

        let globals = self.python_globals.cast_as::<PyDict>(py)?;
        globals.set_item("__input__", input)?;

        let e = |e: PyErr| {
            e.clone_ref(py).print(py);
            e
        };

        let obj = unsafe {
            let ptr = ffi::PyRun_StringFlags(
                "visualize(__input__)\0".as_ptr() as *const _,
                ffi::Py_eval_input,
                self.python_globals.as_ptr(),
                ::std::ptr::null_mut(),
                ::std::ptr::null_mut(),
            );

            PyObject::from_borrowed_ptr_or_err(py, ptr).map_err(e)?
        };

        obj.extract(py).map_err(e)
    }
}
